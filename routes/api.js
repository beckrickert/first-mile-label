var express = require('express');
var router = express.Router();
var slowDown = require('express-slow-down');
var firstmile = require('../fm.js');

var speedLimiter = slowDown({
    windowMs: 1000 * 60,
    delayAfter: 5,
    delayMs: 100
});

router.post('/label', [speedLimiter], (req, res) => {
    //validation
    var v = firstmile.validateLabel(req);
    if (v === true) {
        //sanitize & parse
        var p = firstmile.parseLabel(req.body);

        //generate label
        var g = firstmile.generateLabel(p);
        if (g !== false) {
            res.json({
                status: 200,
                label: g
            });
        }
    }
    else {
        //respond with errors
        res.status(400).json({
            status: 400,
            error: v
        });
    }
});

module.exports = router;