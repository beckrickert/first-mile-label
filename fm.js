var _ = require('underscore');
var emailvalidator = require('email-validator');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var randomstring = require('randomstring');
var validator = require('validator');

var country_prov = [
    {
        country: "",
        states: [""]
    }
];
country_prov = JSON.parse(fs.readFileSync(path.join(__dirname, "./public/country_state_province.json"), "utf8"));

module.exports = {
    parseLabel(labelBody = {
        sender: {
            name: "",
            address: {
                unit: "",
                street_num: "",
                street_name: "",
                city: "",
                state: "",
                postal_zip: "",
                country: ""
            },
            email: "",
            phone: ""
        },
        receiver: {
            name: "",
            address: {
                unit: "",
                street_num: "",
                street_name: "",
                city: "",
                state: "",
                postal_zip: "",
                country: ""
            }
        },
        weight_lbs: 0,
        ship_date: {
            month: 0,
            day: 0,
            year: 0,
        },
        tracking_no: "",
        order_no: "",
        signature_req: false,
        fragile_box: false,
        thissideup_box: false,
        batteries_box: false,
        biohazard_box: false,
        label_logo: null

    }) {
        //trim, titlize
        labelBody.sender.name = validator.escape(validator.stripLow(this.title(validator.trim(labelBody.sender.name))));
        labelBody.receiver.name = validator.escape(validator.stripLow(this.title(validator.trim(labelBody.receiver.name))));

        var senderAddressFull = Object.keys(labelBody.sender.address).map(p => {
            switch (p.toLowerCase()) {
                case "unit":
                case "street_num":
                case "postal_zip":
                    return labelBody.sender.address[p] = validator.escape(validator.stripLow(validator.trim(labelBody.sender.address[p].toUpperCase())));
                case "street_name":
                case "city":
                case "country":
                case "state":
                    if (labelBody.sender.address[p].length >= 2) {
                        return labelBody.sender.address[p] = validator.escape(validator.stripLow(validator.trim(this.title(labelBody.sender.address[p]))));
                    }
                    else {
                        return labelBody.sender.address[p] = validator.escape(validator.stripLow(validator.trim(labelBody.sender.address[p].toUpperCase())));
                    }
            }
        });

        //if unit
        console.log(senderAddressFull[0]);
        if (senderAddressFull[0].length != 0) {
            labelBody.sender.address.full = `${senderAddressFull[0]}-${senderAddressFull[1]} ${senderAddressFull.filter(x => x.length > 0).slice(2).join(", ")}`;

            labelBody.sender.address.full_html = labelBody.sender.address.full.replace(", ", '<br>');

            var s = labelBody.sender.address.full_html.split("<br>");
            var s2 = s[1].indexOf(", ", s[1].indexOf(", ") + 1);
            var s3 = s[1].substring(0, s2) + "<br>" + s[1].substring(s2 + 2);
            labelBody.sender.address.full_html = s[0] + "<br>" + s3;
        }
        //if not unit
        else {
            labelBody.sender.address.full = senderAddressFull.filter(x => x.length > 0).join(", ");
            labelBody.sender.address.full_html = labelBody.sender.address.full.replace(", ", " ");
            labelBody.sender.address.full_html = labelBody.sender.address.full_html.replace(", ", "<br>");
            
            var s = labelBody.sender.address.full_html.split("<br>");
            var s2 = s[1].indexOf(", ", s[1].indexOf(", ") + 1);
            var s3 = s[1].substring(0, s2) + "<br>" + s[1].substring(s2 + 2);
            labelBody.sender.address.full_html = s[0] + "<br>" + s3;
        }

        var receiverAddressFull = Object.keys(labelBody.receiver.address).map(p => {
            switch (p.toLowerCase()) {
                case "unit":
                case "street_num":
                case "postal_zip":
                    return labelBody.receiver.address[p] = validator.escape(validator.stripLow(validator.trim(labelBody.receiver.address[p].toUpperCase())));
                case "street_name":
                case "city":
                case "country":
                case "state":
                    if (labelBody.receiver.address[p].length >= 3) {
                        return labelBody.receiver.address[p] = validator.escape(validator.stripLow(validator.trim(this.title(labelBody.receiver.address[p]))));
                    }
                    else {
                        return labelBody.receiver.address[p] = validator.escape(validator.stripLow(validator.trim(labelBody.receiver.address[p].toUpperCase())));
                    }
            }
        });
        //if unit
        if (receiverAddressFull[0].length != 0) {
            labelBody.receiver.address.full = `${receiverAddressFull[0]}-${receiverAddressFull[1]} ${receiverAddressFull.filter(x => x.length > 0).slice(2).join(", ")}`;

            labelBody.receiver.address.full_html = labelBody.receiver.address.full.replace(", ", '<br>');
            var r = labelBody.receiver.address.full_html.split("<br>");
            var r2 = r[1].indexOf(", ", r[1].indexOf(", ") + 1);
            var r3 = r[1].substring(0, r2) + "<br>" + r[1].substring(r2 + 2);
            labelBody.receiver.address.full_html = r[0] + "<br>" + r3;
        }
        //if no unit
        else {
            labelBody.receiver.address.full = receiverAddressFull.filter(x => x.length > 0).join(", ");
            labelBody.receiver.address.full_html = labelBody.receiver.address.full.replace(", ", " ");
            labelBody.receiver.address.full_html = labelBody.receiver.address.full_html.replace(", ", "<br>");
            
            var r = labelBody.receiver.address.full_html.split("<br>");
            var r2 = r[1].indexOf(", ", r[1].indexOf(", ") + 1);
            var r3 = r[1].substring(0, r2) + "<br>" + r[1].substring(r2 + 2);
            labelBody.receiver.address.full_html = r[0] + "<br>" + r3;
        }
        

        if (emailvalidator.validate(labelBody.sender.email.toLowerCase())) {
            labelBody.sender.email.toLowerCase();
        }
        else {
            labelBody.sender.email = "";
        }

        labelBody.sender.phone = validator.escape(validator.stripLow(validator.trim(labelBody.sender.phone.toString().replace(/ /g, "").replace(/[\(\)]/g, "").replace(/\-/g, ""))));

        labelBody.weight_lbs = validator.escape(validator.stripLow(validator.trim(labelBody.weight_lbs.toString().replace(/[a-zA-Z]/g, "").replace(/\-/g, ""))));

        var tempDate = labelBody.ship_date.year + "-" + (labelBody.ship_date.month++).toString() + "-" + labelBody.ship_date.day;
        labelBody.ship_date_unix = moment(tempDate, "YYYY-MM-DD").format("X");
        labelBody.ship_date.year = labelBody.ship_date.year.toString();
        labelBody.ship_date.month = labelBody.ship_date.month.toString();
        labelBody.ship_date.day = labelBody.ship_date.day.toString();

        labelBody.tracking_no = validator.escape(validator.stripLow(validator.trim(labelBody.tracking_no.replace(/[^A-Za-z0-9]/g, ''))));

        labelBody.order_no = validator.escape(validator.stripLow(validator.trim(labelBody.order_no.replace(/[^A-Za-z0-9]/g, ''))));

        if (labelBody.signature_req) {
            if (labelBody.signature_req == "on") {
                labelBody.signature_req = true;
            }
            else {
                labelBody.signature_req = false;
            }
        }
        else {
            labelBody.signature_req = false;
        }

        if (labelBody.fragile_box) {
            if (labelBody.fragile_box == "on") {
                labelBody.fragile_box = true;
            }
            else {
                labelBody.fragile_box = false;
            }
        }
        else {
            labelBody.fragile_box = false;
        }

        if (labelBody.batteries_box) {
            if (labelBody.batteries_box == "on") {
                labelBody.batteries_box = true;
            }
            else {
                labelBody.batteries_box = false;
            }
        }
        else {
            labelBody.batteries_box = false;
        }

        if (labelBody.thissideup_box) {
            if (labelBody.thissideup_box == "on") {
                labelBody.thissideup_box = true;
            }
            else {
                labelBody.thissideup_box = false;
            }
        }
        else {
            labelBody.thissideup_box = false;
        }

        if (labelBody.biohazard_box) {
            if (labelBody.biohazard_box == "on") {
                labelBody.biohazard_box = true;
            }
            else {
                labelBody.biohazard_box = false;
            }
        }
        else {
            labelBody.biohazard_box = false;
        }


        return labelBody;

    },
    formatPostalZipcode(postal) {
        return postal.replace(/ /g, "").toUpperCase();
    },
    title(name) {
        var n = name.split(" ");
        return n.map((x) => x.charAt(0).toUpperCase() + x.substring(1).toLowerCase()).join(" ");
    },
    isEmail(email) {
        return emailvalidator.validate(email.toString().toLowerCase());
    },
    countries: country_prov,
    generateLabel(labelBody = {
        sender: {
            name: "",
            address: {
                unit: "",
                street_num: "",
                street_name: "",
                city: "",
                state: "",
                postal_zip: "",
                country: "",
                full: "",
                full_html: ""
            },
            email: "",
            phone: ""
        },
        receiver: {
            name: "",
            address: {
                unit: "",
                street_num: "",
                street_name: "",
                city: "",
                state: "",
                postal_zip: "",
                country: "",
                full: "",
                full_html: ""
            }
        },
        weight_lbs: 0,
        ship_date: {
            month: 0,
            day: 0,
            year: 0,
        },
        ship_date_unix: 0,
        tracking_no: "",
        order_no: "",
        signature_req: false,
        fragile_box: false,
        thissideup_box: false,
        batteries_box: false,
        biohazard_box: false,
        label_logo: null
    }) {
        var f = fs.readFileSync(path.join(__dirname, "public", "label_template.html"), "utf8");
        var labelid = randomstring.generate({readable: true, capitalization: "lowercase"});
        f = f.replace(/%LABELID%/g, labelid);
        
        f = f.replace(/%SENDERNAME%/g, labelBody.sender.name);
        f = f.replace(/%SENDERADDRESSFULLHTML%/g, labelBody.sender.address.full_html);
        f = f.replace(/%SENDEREMAIL%/g, labelBody.sender.email);
        f = f.replace(/%SENDERPHONE%/g, labelBody.sender.phone);

        f = f.replace(/%SHIPDATE%/g, moment(labelBody.ship_date_unix, "X").format("MMM-DD-YYYY"));
        if (labelBody.weight_lbs != 0 || labelBody.weight_lbs.length != 0) {
            f = f.replace('id="weightorder"', "");
            f = f.replace(/%WEIGHTLBS%/g, labelBody.weight_lbs);
        }
        
        if (labelBody.signature_req) {
            f = f.replace(/%SIGNATURE%/g, "YES");
        }
        else {
            f = f.replace(/%SIGNATURE%/g, "NO");
        }


        f = f.replace(/%RECEIVERNAME%/g, labelBody.receiver.name);
        f = f.replace(/%RECEIVERADDRESSFULLHTML%/g, labelBody.receiver.address.full_html);

        if (labelBody.tracking_no.length != 0) {
            f = f.replace('id="trackingno"', "");
            f = f.replace(/%TRACKINGNO%/g, labelBody.tracking_no);
        }

        if (labelBody.order_no.length != 0) {
            f = f.replace('id="orderno"', "");
            f = f.replace(/%ORDERNO%/g, labelBody.order_no);
        }

        if (labelBody.fragile_box) {
            f = f.replace('id="fragile"', "");
        }

        if (labelBody.thissideup_box) {
            f = f.replace('id="thissideup"', "");
        }

        if (labelBody.batteries_box) {
            f = f.replace('id="batteries"', "");
        }

        if (labelBody.biohazard_box) {
            f = f.replace('id="biohazard"', "");
        }

        /* //label_logo
        if (labelBody.label_logo != null) {
            f = f.replace('id="biohazard"', "");
            f = f.replace('%LABELLOGO%', `../logos/${labelBody.label_logo.name}`);
        } */

        //save to file
        try {
            fs.writeFileSync(path.join(__dirname, "public", "labels", `${labelid}.html`), f);
            
            console.log("Written label to file. " + labelid);
            return labelid + ".html";
        }
        catch (err) {
            console.log(err);
            return false;
        }

    },
    validateLabel(req) {
        var errorsArr = [];

        //sender address
        Object.keys(req.body.sender.address).forEach(k => {
            switch (k.toLowerCase()) {
                case "unit":
                    if (!validator.isEmpty(req.body.sender.address[k].toString(), {ignore_whitespace: true})) {
                        if (!(
                            validator.isAscii(req.body.sender.address[k]) || 
                            validator.isAlphanumeric(req.body.sender.address[k])
                            )) {
                                errorsArr.push("sender_address_" + k);
                        }
                    }
                    break;

                case "street_num":
                case "street_name":
                case "postal_zip":
                    if (!validator.isEmpty(req.body.sender.address[k].toString(), {ignore_whitespace: true})) {
                        if (!(
                            validator.isAscii(req.body.sender.address[k]) || 
                            validator.isAlphanumeric(req.body.sender.address[k])
                            )) {
                                errorsArr.push("sender_address_" + k);
                        }
                    }
                    else {
                        errorsArr.push("sender_address_" + k);
                    }
                    break;

                case "city":
                case "country":
                    if (!(validator.isEmpty(req.body.sender.address[k].toString(), {ignore_whitespace: true}) || req.body.sender.address[k].toString().toLowerCase() == "null")) {
                        if (!validator.isAscii(req.body.sender.address[k])) {
                            errorsArr.push("sender_address_" + k);
                        }
                    }
                    else {
                        errorsArr.push("sender_address_" + k);
                    }
                    break;

                case "state":
                    if (!(validator.isEmpty(req.body.sender.address[k].toString(), {ignore_whitespace: true}) || req.body.sender.address[k].toString().toLowerCase() == "null")) {
                            if (!validator.isAscii(req.body.sender.address[k])) {
                                errorsArr.push("sender_address_" + k);
                            }
                        }
                        else {
                            errorsArr.push("sender_address_" + k);
                        }
                    break;

            }
        });

        //sender name
        if (!validator.isAscii(req.body.sender.name)) {
            errorsArr.push("sender_name");
        }

        //sender email
        if (!(validator.isAscii(req.body.sender.email) || validator.isEmail(req.body.sender.email))) {
            errorsArr.push("sender_email");
        }

        //sender phone
        if (!(validator.isAscii(req.body.sender.phone) || validator.isMobilePhone(req.body.sender.phone.toString()))) {
            errorsArr.push("sender_phone");
        }

        //receiver address
        Object.keys(req.body.receiver.address).forEach(k => {
            switch (k.toLowerCase()) {
                case "unit":
                    if (!validator.isEmpty(req.body.receiver.address[k], {ignore_whitespace: true})) {
                        if (!(
                            validator.isAscii(req.body.receiver.address[k]) || 
                            validator.isAlphanumeric(req.body.receiver.address[k])
                            )) {
                                errorsArr.push("receiver_address_" + k);
                        }
                    }
                    break;

                case "street_num":
                case "street_name":
                case "postal_zip":
                    if (!validator.isEmpty(req.body.receiver.address[k], {ignore_whitespace: true})) {
                        if (!(
                            validator.isAscii(req.body.receiver.address[k]) || 
                            validator.isAlphanumeric(req.body.receiver.address[k])
                            )) {
                                errorsArr.push("receiver_address_" + k);
                        }
                    }
                    else {
                        errorsArr.push("receiver_address_" + k);
                    }
                    break;

                case "city":
                case "country":
                    if (!(validator.isEmpty(req.body.receiver.address[k].toString(), {ignore_whitespace: true}) || req.body.receiver.address[k].toString().toLowerCase() == "null")) {
                        if (!validator.isAscii(req.body.receiver.address[k])) {
                            errorsArr.push("receiver_address_" + k);
                        }
                    }
                    else {
                        errorsArr.push("receiver_address_" + k);
                    }
                    break;

                case "state":
                    if (!(validator.isEmpty(req.body.receiver.address[k].toString(), {ignore_whitespace: true}) || req.body.receiver.address[k].toString().toLowerCase() == "null")) {
                            if (!validator.isAscii(req.body.receiver.address[k])) {
                                errorsArr.push("receiver_address_" + k);
                            }
                        }
                        else {
                            errorsArr.push("receiver_address_" + k);
                        }
                        break;

            }
        });

        //receiver name
        if (!validator.isAscii(req.body.receiver.name)) {
            errorsArr.push("receiver_name");
        }

        //ship_date
        Object.keys(req.body.ship_date).forEach(k => {
            switch (k.toLowerCase()) {
                case "month":
                    if (!validator.isInt(req.body.ship_date[k].toString(), {
                        min: 0,
                        max: 11,
                        allow_leading_zeroes: true
                    })) {
                        errorsArr.push("ship_date_" + k);
                    }
                    break;

                case "day":
                    if (!validator.isInt(req.body.ship_date[k].toString(), {
                        min: 1,
                        max: 31,
                        allow_leading_zeroes: true
                    })) {
                        errorsArr.ship_date.push("ship_date_" + k);
                    }
                    break;
                
                case "year":
                    if (!validator.isInt(req.body.ship_date[k].toString(), {
                        min: 1900,
                        max: 9999,
                        allow_leading_zeroes: false
                    })) {
                        errorsArr.ship_date.push("ship_date_" + k);
                    }
                    break;
            }
        });

        //weight
        if (!validator.isEmpty(req.body.weight_lbs.toString(), {ignore_whitespace: true})) {
            if (!(validator.isNumeric(req.body.weight_lbs.toString(), {min: 0, max: 9999999999}))) {
                errorsArr.push("weight");
            }
        }

        //tracking & order
        if (!validator.isEmpty(req.body.tracking_no.toString(), {ignore_whitespace: true})) {
            if (!validator.isAlphanumeric(req.body.tracking_no)) {
                errorsArr.push("tracking_no");
            }
        }

        if (!validator.isEmpty(req.body.order_no.toString(), {ignore_whitespace: true})) {
            if (!validator.isAlphanumeric(req.body.order_no)) {
                errorsArr.push("order_no");
            }
        }

        //signature
        if (req.body.signature_req) {
            if (!["on", "off"].includes(req.body.signature_req.toLowerCase())) {
                errorsArr.push("signature_req");
            }
        }

        //fragile
        if (req.body.fragile_box) {
            if (!["on", "off"].includes(req.body.fragile_box.toLowerCase())) {
                errorsArr.push("fragile_box");
            }
        }

        //thissideup
        if (req.body.thissideup_box) {
            if (!["on", "off"].includes(req.body.thissideup_box.toLowerCase())) {
                errorsArr.push("thissideup_box");
            }
        }

        //batteries
        if (req.body.batteries_box) {
            if (!["on", "off"].includes(req.body.batteries_box.toLowerCase())) {
                errorsArr.push("batteries_box");
            }
        }

        //biohazard
        if (req.body.biohazard_box) {
            if (!["on", "off"].includes(req.body.biohazard_box.toLowerCase())) {
                errorsArr.push("biohazard_box");
            }
        }

        //label_logo
        /* if (req.files.label_logo) {
            var logo = req.files.label_logo;
            if (![
                "image/png", 
                "image/jpg", 
                "image/jpeg", 
                "image/bmp", 
                "image/gif"
            ].includes(logo.mimetype.toLowerCase())) {
                errorsArr.push("logo_label");
            }
        } */

        if (errorsArr.length > 0) {
            return errorsArr;
        }
        else {
            return true;
        }
    }
}