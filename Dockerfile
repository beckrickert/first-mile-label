FROM node:12.16.3-slim

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# If you are building your code for production
RUN npm install --only=production

# If you are building your code for development
# RUN npm install

# Copy app sourcecode current folder where this dockerfile is to working directory
COPY . .

EXPOSE 8099

CMD ["node", "main.js"]

#docker build -t rickert/firstmilelabel:latest .