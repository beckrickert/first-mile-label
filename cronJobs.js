var CronJob = require('cron').CronJob;
var fse = require('fs-extra');
var path = require('path');
var findRemoveSync = require('find-remove');

var cleanUpLabels = (p) => {
    console.log("Cleaning up generated label files.");
    p = (p == undefined) ? path.join(__dirname, "public", "labels") : p;

    //delete all labels older than 1 hour
    var deleted = findRemoveSync(p, {
        age: {
            seconds: 3600
        },
        extensions: '.html'
    });
}

/* var cleanUpLogos = (p) => {
    console.log("Cleaning up user uploaded logo files.");
    p = (p == undefined) ? path.join(__dirname, "public", "logos") : p;

    //delete all labels older than 1 hour
    var deleted = findRemoveSync(p, {
        age: {
            seconds: 3600
        },
        extensions: ['.png', '.jpg', '.jpeg', '.bmp', '.gif']
    });
} */

module.exports = {
    EveryHourJobs: () => {
        return new CronJob('00 00 * * * *', () => {
            cleanUpLabels();
            //cleanUpLogos();
        }, null, true, process.env.TZ);
    }
}